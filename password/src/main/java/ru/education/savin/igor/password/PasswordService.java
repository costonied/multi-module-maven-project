package ru.education.savin.igor.password;

public interface PasswordService {
    String hash(String input);
    String algorithm();
}
